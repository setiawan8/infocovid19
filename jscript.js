$.ajax({
    url         : "https://api.kawalcorona.com/indonesia/provinsi/",
    type        : "GET",
    dataType    : "json",
    data        : {get_param : 'value'},
    success     : function(data){
        //menghitung jumlah data
        jmlData = data.length;
        
        //variabel untuk menampung tabel yang akan digenerasikan
        buatTabel = "";
        
        //perulangan untuk menayangkan data dalam tabel
        for(a = 0; a < jmlData; a++){
            
            //mencetak baris baru
            buatTabel += "<tr>"
            
                        //membuat penomoran
                        + "<td>" + (a+1) + "</td>"
                        //mencetak Provinsi
                        + "<td>" + data[a]["attributes"]["Provinsi"] + "</td>"
                        //mencetak Kasus positif
                        + "<td>" + data[a]["attributes"]["Kasus_Posi"] + "</td>"
                        //mencetak Kasus sembuh
                        + "<td>" + data[a]["attributes"]["Kasus_Semb"] + "</td>"
                        //mencetak Kasus Meninggal
                        + "<td>" + data[a]["attributes"]["Kasus_Meni"] + "</td>"
            //tutup baris baru
                + "<tr/>";
        }
        //mencetak tabel
        document.getElementsByTagName("table")[0].innerHTML += buatTabel;
    }
});